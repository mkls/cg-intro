use std::mem;
use std::ffi::CString;
use gl::types::*;
use crate::gl_helper::{compile_shader, link_shader_program};

pub struct Drawable {
    vao: GLuint,
    vbo: GLuint,
    vio: GLuint,

    element_count: u32,

    pub g_mvp: GLint,
    pub g_mv: GLint,

    program: GLuint
}

impl Drawable {
    pub fn new(vs_src: &str, fs_src: &str, vertices: Vec<f32>, indices: Vec<u32>) -> Self {
        // Compile shader program for the new drawable
        let vs = compile_shader(vs_src, gl::VERTEX_SHADER);
        let fs = compile_shader(fs_src, gl::FRAGMENT_SHADER);

        let program = link_shader_program(vs, fs);

        // Generate array and buffer objects
        let mut vao = 0;
        let mut vbo = 0;
        let mut vio = 0;

        let g_mvp;
        let g_mv;

        unsafe {
            // Vertices
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);

            gl::GenBuffers(1, &mut vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                vertices.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW
            );

            // Indices
            gl::GenBuffers(1, &mut vio);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, vio);
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (indices.len() * mem::size_of::<GLuint>()) as GLsizeiptr,
                indices.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW
            );


            // Attributes
            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0,
                2,
                gl::FLOAT,
                gl::FALSE,
                2 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null()
            );

            g_mvp = gl::GetUniformLocation(program,
                CString::new("mvp").unwrap().as_bytes_with_nul().as_ptr() as *const i8
            );

            g_mv = gl::GetUniformLocation(program,
                CString::new("mv").unwrap().as_bytes_with_nul().as_ptr() as *const i8
            );
        }

        Self {
            vao,
            vbo,
            vio,
            g_mvp,
            g_mv,
            element_count: indices.len() as u32,
            program
        }
    }

    pub fn draw(&self){
        unsafe {
            gl::UseProgram(self.program);

            gl::BindVertexArray(self.vao);

            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.vio);

            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0,
                2,
                gl::FLOAT,
                gl::FALSE,
                2 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null()
            );

            gl::DrawElements(
                gl::TRIANGLES,
                self.element_count as i32,
                gl::UNSIGNED_INT,
                std::ptr::null()
            );

            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }
    }
}

impl Drop for Drawable {
    fn drop(&mut self) {
        let buffers = vec![self.vbo, self.vio];

        unsafe {
            gl::DeleteBuffers(buffers.len() as i32, buffers.as_ptr() as *const GLuint);
            gl::DeleteVertexArrays(1, vec![self.vao].as_ptr() as *const GLuint);
            gl::DeleteProgram(self.program);
        }
    }
}
