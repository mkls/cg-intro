use std::{ffi::CString,
          str,
          ptr};
use gl::types::*;

pub fn compile_shader(src: &str, ty: GLenum) -> GLuint {
    let shader;

    unsafe {
        shader = gl::CreateShader(ty);

        let c_src = CString::new(src.as_bytes());

        match c_src {
            Ok(c_src) => gl::ShaderSource(shader, 1, &c_src.as_ptr(), ptr::null()),
            Err(e) => panic!("Error while reading shader source.\nReason: {}", e)
        }

        gl::CompileShader(shader);

        let mut status = gl::FALSE as GLint;
        gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);

        if status != (gl::TRUE as GLint) {
                let mut len = 0;
                gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut len);

                let mut buf = Vec::with_capacity(len as usize);
                buf.set_len((len as usize) - 1);

                gl::GetShaderInfoLog(shader, len, ptr::null_mut(), 
                                     buf.as_mut_ptr() as *mut GLchar);
                panic!("{}", str::from_utf8(&buf)
                             .ok()
                             .expect("ShaderInfoLog not valid utf8"));
        }
    }

    shader
}

pub fn link_shader_program(vs: GLuint, fs: GLuint) -> GLuint {
    let program;

    unsafe {
        program = gl::CreateProgram();

        gl::AttachShader(program, vs);
        gl::AttachShader(program, fs);

        gl::LinkProgram(program);

        gl::DetachShader(program, vs);
        gl::DetachShader(program, fs);

        gl::DeleteShader(vs);
        gl::DeleteShader(fs);

        let mut status = gl::FALSE as GLint;
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);

        if status != (gl::TRUE as GLint) {
            let mut len: GLint = 0;
            gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut len);

            let mut buf = Vec::with_capacity(len as usize);
            buf.set_len((len as usize) - 1);

            gl::GetProgramInfoLog(program, len, ptr::null_mut(), 
                                  buf.as_mut_ptr() as *mut GLchar);
            panic!("{}", str::from_utf8(&buf)
                              .ok()
                              .expect("ShaderInfoLog not valid utf8"));
        }
    }

    program
}
