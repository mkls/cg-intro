#version 330 core

in vec3 normal;
in vec3 pos;
out vec4 f_color;

void main() {
    // const vec3 E = vec3(0.0, 0.0, 0.0);
    const vec3 light_pos = vec3(100.0, 50.0, 0.0);
    const vec3 light_color = vec3(0.93, 0.79, 0.69);
    const float gamma_level = 2.2;
    const float shinness = 20.0;

    vec3 normalized_normal = normalize(normal);
    vec3 light_direction = normalize(light_pos - pos);
    vec3 view_direction = normalize(-pos);
    vec3 h = normalize(light_direction + view_direction);

    float d = max(dot(normal, light_direction), 0.05);
    float specular = pow(max(dot(normalized_normal, h), 0.00), shinness);

    vec3 frag_color = light_color * d + vec3(specular);

    f_color = vec4(pow(frag_color, vec3(1.0 / gamma_level)), 1.0);
    // f_color = vec4(normal, 1.0);
}
