#version 330 core

out vec4 f_color;

void main() {
    f_color = vec4(0.3, 0.5, 0.9, 1.0);
}
