#version 330 core

in vec3 v_normal;
in vec3 v_pos;
out vec4 f_color;

void main() {
    const vec3 light_pos = vec3(10.0, 10.0, 10.0);
    // const vec3 light_color = vec3(0.93, 0.79, 0.69);
    const vec3 light_color = vec3(0.5, 0.1, 0.8);
    const float gamma_level = 2.2;
    const float shinness = 20.0;

    vec3 normalized_normal = normalize(v_normal);
    vec3 light_direction = normalize(light_pos - v_pos);
    vec3 view_direction = normalize(-v_pos);
    vec3 h = normalize(light_direction + view_direction);

    float d = max(dot(v_normal, light_direction), 0.05);
    float specular = pow(max(dot(normalized_normal, h), 0.00), shinness);

    vec3 frag_color = light_color * d + vec3(specular);

    f_color = vec4(pow(frag_color, vec3(1.0 / gamma_level)), 1.0);
    // f_color = vec4(normalized_normal, 1.0);
    // f_color = vec4(0.5, 0.1, 0.8, 1.0);
}
