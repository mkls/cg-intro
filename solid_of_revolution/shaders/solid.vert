#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

out vec3 v_normal;
out vec3 v_pos;

uniform mat4 mvp;
uniform mat4 mv;
uniform mat3 mn;

void main() {
    v_pos = vec3(mv * vec4(position, 1.0));
    v_normal = normalize(mn * normal); //mat3(mv) * normal;
    // v_normal = normalize(transpose(inverse(mat3(mv))) * normal);

    gl_Position = mvp * vec4(position, 1.0);
    // gl_Position = vec4(v_pos, 1.0);
}
