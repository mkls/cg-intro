use crate::curve::Curve;
use crate::draw::Draw;
use crate::point::Point;
use crate::solid::Solid;
use cgmath::{perspective, point3, vec3, Deg, Matrix4, Rad, SquareMatrix};
use gl;
use glutin;
use glutin::{
    dpi::{LogicalSize, PhysicalPosition},
    event::{ElementState, Event, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
    ContextBuilder, WindowedContext,
};
use std::time::Instant;

pub struct Application {
    context: Option<WindowedContext<glutin::NotCurrent>>,
    event_loop: Option<EventLoop<()>>,
}

pub struct ApplicationBuilder {
    size: LogicalSize<u16>,
    title: String,
    resizable: bool,
}

impl ApplicationBuilder {
    pub fn new() -> ApplicationBuilder {
        Self {
            size: LogicalSize::new(800, 600),
            title: String::from("CG task"),
            resizable: false,
        }
    }

    pub fn with_size(&mut self, size: LogicalSize<u16>) -> &mut Self {
        self.size = size;
        self
    }

    pub fn with_title(&mut self, title: String) -> &mut Self {
        self.title = title;
        self
    }

    pub fn with_resizable(&mut self, resizable: bool) -> &mut Self {
        self.resizable = resizable;
        self
    }

    pub fn build(&self) -> Application {
        let event_loop = EventLoop::new();

        let window = WindowBuilder::new()
            .with_inner_size(self.size)
            .with_title(&self.title)
            .with_resizable(self.resizable);

        let context = ContextBuilder::new()
            .with_gl(glutin::GlRequest::Specific(glutin::Api::OpenGl, (3, 3)))
            .with_gl_profile(glutin::GlProfile::Core)
            .with_pixel_format(24, 8)
            .with_srgb(false)
            .with_vsync(true)
            .build_windowed(window, &event_loop);

        let actual_window = match context {
            Ok(win) => win,
            Err(err) => panic!("Error occured while creating window.\nReason: {}", err),
        };

        Application {
            context: Some(actual_window),
            event_loop: Some(event_loop),
        }
    }
}

impl Application {
    pub fn run(&mut self) {
        let gl_window = unsafe {
            self.context.take().unwrap().make_current().unwrap()
        };

        gl::load_with(|symbol| gl_window.get_proc_address(symbol));

        unsafe {
            gl::Enable(gl::DEPTH_TEST);
        }

        let handler = self.event_loop.take().unwrap();

        // let mut points = Vec::new();
        let mut cursor_pos = PhysicalPosition::new(0.0, 0.0);
        let mut win_size = gl_window.window().inner_size();
        let mut curve = Curve::new().unwrap();
        let mut solid = Solid::new().unwrap();
        let mut mode = false;

        let mut model: Matrix4<f32> = Matrix4::identity();
        let view: Matrix4<f32> = Matrix4::look_at_rh(
            point3(0.0, 0.0, -5.0),
            point3(0.0, 0.0, 0.0),
            vec3(0.0, 1.0, 0.0),
        );
        let proj: Matrix4<f32> = perspective(Rad::from(Deg(45.0)), 1.0, 0.1, 100.0);
        let mut moment = Instant::now();

        handler.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Wait;

            match event {
                Event::WindowEvent { event, .. } => match event {
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                    WindowEvent::Resized(size) => {
                        gl_window.resize(size);
                        win_size = size;
                        unsafe { gl::Viewport(0, 0, size.width as i32, size.height as i32) }
                    }
                    WindowEvent::CursorMoved { position: pos, .. } => {
                        cursor_pos = pos;
                    }
                    WindowEvent::KeyboardInput { input, .. } => {
                        if input.virtual_keycode.unwrap() == VirtualKeyCode::Space
                            && input.state == ElementState::Pressed
                        {
                            mode ^= true;
                            solid.set_from_curve(&curve, 128);
                        } else if input.virtual_keycode.unwrap() == VirtualKeyCode::Delete && input.state == ElementState::Pressed && !mode {
                            curve.reset();
                        }
                    }
                    WindowEvent::MouseInput { state, button, .. } => {
                        if button == glutin::event::MouseButton::Left
                            && state == glutin::event::ElementState::Pressed
                        {
                            let x = cursor_pos.x as f32 / win_size.width as f32 * 2.0 - 1.0;
                            let y =
                                (cursor_pos.y as f32 / win_size.height as f32 * 2.0 - 1.0) * -1.0;
                            if !mode && y <= 0.0 {
                                curve.add_point(Point::new(x, y, 2.0).unwrap());
                            }
                        }
                    }
                    _ => (),
                },
                Event::RedrawRequested(_) => {
                    unsafe {
                        gl::ClearColor(0.9, 0.9, 0.9, 1.0);
                        gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
                    }

                    // Call redraw here
                    if mode {
                        let rot =
                            Matrix4::from_angle_y(Deg(moment.elapsed().as_millis() as f32 / 10.0));
                        model = rot * model;
                        solid.draw(Some(&model), Some(&view), Some(&proj));
                        moment = Instant::now();
                    } else {
                        curve.draw(None, None, None);
                    }
                    gl_window.swap_buffers().unwrap();
                }
                _ => (),
            }
            gl_window.window().request_redraw();
        });
    }
}
