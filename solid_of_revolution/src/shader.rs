use gl::types::*;
use std::ffi::CString;
use std::fs::read_to_string;
use std::ptr;
use std::str;

/// OpenGL shader handler structure
pub struct Shader {
    pub(super) handle: GLuint,
}

impl Shader {
    /// Create new shader of type `ty` object from source located at `path`
    pub fn new(path: &str, ty: GLenum) -> Result<Self, std::io::Error> {
        let shader_source = read_to_string(path)?;
        let handle = Self::compile_shader(shader_source.as_str(), ty);

        Ok(Self { handle })
    }

    /// Private function used to compile GLSL shaders
    fn compile_shader(src: &str, ty: GLenum) -> GLuint {
        let shader;

        unsafe {
            shader = gl::CreateShader(ty);

            let c_src = CString::new(src.as_bytes());

            match c_src {
                Ok(c_src) => gl::ShaderSource(shader, 1, &c_src.as_ptr(), ptr::null()),
                Err(e) => panic!("Error while reading shader source.\nReason: {}", e),
            }

            gl::CompileShader(shader);

            let mut status = gl::FALSE as GLint;
            gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);

            if status != (gl::TRUE as GLint) {
                let mut len = 0;
                gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut len);

                let mut buf = Vec::with_capacity(len as usize);
                buf.set_len((len as usize) - 1);

                gl::GetShaderInfoLog(
                    shader,
                    len,
                    ptr::null_mut(),
                    buf.as_mut_ptr() as *mut GLchar,
                );
                panic!(
                    "{}",
                    str::from_utf8(&buf)
                        .ok()
                        .expect("ShaderInfoLog not valid utf8")
                );
            }
        }

        shader
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.handle);
        }
    }
}
