mod app;
mod curve;
mod draw;
mod point;
mod program;
mod shader;
mod solid;

use app::ApplicationBuilder;
use glutin::dpi::LogicalSize;

fn main() {
    let mut application = ApplicationBuilder::new()
        .with_title(String::from("Solid of revolution"))
        .with_size(LogicalSize::new(800, 800))
        .with_resizable(false)
        .build();

    application.run();
}
