use cgmath::Matrix4;

pub trait Draw {
    fn draw(
        &self,
        model: Option<&Matrix4<f32>>,
        view: Option<&Matrix4<f32>>,
        proj: Option<&Matrix4<f32>>,
    );
}
