use crate::curve::Curve;
use crate::draw::Draw;
use crate::program::ShaderProgram;
use crate::shader::Shader;
use cgmath::prelude::Matrix;
use cgmath::{perspective, point3, vec3, Deg, Matrix3, Matrix4, Rad, SquareMatrix};
use gl::types::{GLfloat, GLint, GLsizeiptr, GLuint, GLvoid};
use gl::{FRAGMENT_SHADER, VERTEX_SHADER};
use std::env::current_exe;
use std::f32::consts::PI;
use std::ffi::CString;
use std::mem;

pub struct Solid {
    vao: GLuint,
    vbo: GLuint,
    vio: GLuint,

    u_mvp: GLint,
    u_mv: GLint,
    u_mn: GLint,

    vertices: Vec<f32>,
    indices: Vec<u32>,

    program: ShaderProgram,
}

impl Solid {
    pub fn new() -> Result<Self, std::io::Error> {
        let exe_path = current_exe().unwrap().parent().unwrap().join("shaders");

        let vs_path = exe_path.join("solid.vert");
        let fs_path = exe_path.join("solid.frag");

        let vs = Shader::new(vs_path.to_str().unwrap(), VERTEX_SHADER)?;
        let fs = Shader::new(fs_path.to_str().unwrap(), FRAGMENT_SHADER)?;

        let program = ShaderProgram::new(vec![vs, fs]);

        let vertices = Vec::new();
        let indices = Vec::new();

        let vao = 0;
        let vbo = 0;
        let vio = 0;
        let u_mvp = 0;
        let u_mv = 0;
        let u_mn = 0;

        let mut solid = Self {
            vao,
            vbo,
            vio,
            u_mvp,
            u_mv,
            u_mn,
            vertices,
            indices,
            program,
        };

        solid.make_buffers();

        Ok(solid)
    }

    pub fn set_from_curve(&mut self, curve: &Curve, rots: u32) {
        let curve_points = &curve.curve_points;
        let curve_normals = &curve.curve_normals;

        let mut rot_angle = 0.0 as f32;

        let mut solid_vertices: Vec<f32> = Vec::with_capacity(3 * curve_points.len());
        let solid_indices;

        for _ in 0..rots {
            let rotation = Matrix3::from_angle_x(Rad(rot_angle));

            for j in (0..curve_points.len()).step_by(2) {
                let vertex_vec = rotation * vec3(curve_points[j], curve_points[j + 1], 0.0);
                let normal_vec = rotation * vec3(curve_normals[j], curve_normals[j + 1], 0.0);

                solid_vertices.push(vertex_vec.x);
                solid_vertices.push(vertex_vec.y);
                solid_vertices.push(vertex_vec.z);

                solid_vertices.push(-normal_vec.x);
                solid_vertices.push(-normal_vec.y);
                solid_vertices.push(-normal_vec.z);
            }

            rot_angle += 2.0 * PI / rots as f32;
        }

        solid_indices = Self::make_indices(curve_points.len() / 2, rots as usize);

        self.vertices = solid_vertices;
        self.indices = solid_indices;

        self.update_buffers();
    }

    fn make_indices(n: usize, rots: usize) -> Vec<u32> {
        let mut indices = Vec::with_capacity((n - 1) * rots * 6);

        let mut i = 0;
        let mut k = 0;

        while i < (n - 1) * (rots - 1) * 6 {
            let mut count = 0;

            while count < n - 1 {
                indices.push(k as u32);
                indices.push(k + n as u32);
                indices.push(k + 1);
                indices.push(k + n as u32);
                indices.push((k + 1) + n as u32);
                indices.push(k + 1);

                count += 1;
                i += 6;
                k += 1;
            }
            k += 1;
        }

        let mut c = 0 as u32;
        k = n as u32 * (rots as u32 - 1);

        while c < (n - 1) as u32 {
            indices.push(c);
            indices.push(c + k + 1);
            indices.push(c + k);
            indices.push(c);
            indices.push(c + 1);
            indices.push(c + k + 1);

            c += 1;
            i += 6;
        }

        indices
    }

    fn update_buffers(&mut self) {
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (self.vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                self.vertices.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );

            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.vio);
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (self.indices.len() * mem::size_of::<GLuint>()) as GLsizeiptr,
                self.indices.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );
        }
    }

    fn make_buffers(&mut self) {
        unsafe {
            // Vertices
            gl::GenVertexArrays(1, &mut self.vao);
            gl::BindVertexArray(self.vao);

            gl::GenBuffers(1, &mut self.vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (self.vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                self.vertices.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );

            // Indices
            gl::GenBuffers(1, &mut self.vio);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.vio);
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (self.indices.len() * mem::size_of::<GLuint>()) as GLsizeiptr,
                self.indices.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );

            // Attributes
            gl::EnableVertexAttribArray(0);
            // Vertex coordinates
            gl::VertexAttribPointer(
                0,
                3,
                gl::FLOAT,
                gl::FALSE,
                3 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null(),
            );
            gl::EnableVertexAttribArray(1);
            // Normals
            gl::VertexAttribPointer(
                1,
                3,
                gl::FLOAT,
                gl::FALSE,
                3 * mem::size_of::<GLfloat>() as GLint,
                (3 * mem::size_of::<GLfloat>()) as *const GLvoid,
            );

            // Uniforms
            self.u_mvp = gl::GetUniformLocation(
                self.program.handle,
                CString::new("mvp").unwrap().as_bytes_with_nul().as_ptr() as *const i8,
            );

            self.u_mv = gl::GetUniformLocation(
                self.program.handle,
                CString::new("mv").unwrap().as_bytes_with_nul().as_ptr() as *const i8,
            );

            self.u_mn = gl::GetUniformLocation(
                self.program.handle,
                CString::new("mn").unwrap().as_bytes_with_nul().as_ptr() as *const i8,
            );
        }
    }
}

impl Draw for Solid {
    fn draw(
        &self,
        model: Option<&Matrix4<f32>>,
        view: Option<&Matrix4<f32>>,
        proj: Option<&Matrix4<f32>>,
    ) {
        let mvp;
        let mv;
        let mn;

        if model.is_some() && view.is_some() && proj.is_some() {
            mvp = proj.unwrap() * view.unwrap() * model.unwrap();
            mv = view.unwrap() * model.unwrap();
        } else {
            let model: Matrix4<f32> = Matrix4::identity();

            let view: Matrix4<f32> = Matrix4::look_at_rh(
                point3(0.0, 0.0, -3.0),
                point3(0.0, 0.0, 0.0),
                vec3(0.0, 1.0, 0.0),
            );

            let proj: Matrix4<f32> = perspective(Rad::from(Deg(45.0)), 1.0, 0.1, 100.0);

            mvp = proj * view * model;
            mv = view * model;
        }

        let x = vec3(mv.x.x, mv.x.y, mv.x.z);
        let y = vec3(mv.y.x, mv.y.y, mv.y.z);
        let z = vec3(mv.z.x, mv.z.y, mv.z.z);

        mn = Matrix3::from_cols(x, y, z).invert().unwrap().transpose();

        unsafe {
            gl::UseProgram(self.program.handle);
            gl::BindVertexArray(self.vao);

            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.vio);

            gl::UniformMatrix4fv(self.u_mvp, 1, gl::FALSE, mvp.as_ptr());
            gl::UniformMatrix4fv(self.u_mv, 1, gl::FALSE, mv.as_ptr());
            gl::UniformMatrix3fv(self.u_mn, 1, gl::FALSE, mn.as_ptr());

            gl::EnableVertexAttribArray(0);
            // Vertex coordinates
            gl::VertexAttribPointer(
                0,
                3,
                gl::FLOAT,
                gl::FALSE,
                6 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null(),
            );
            gl::EnableVertexAttribArray(1);
            // Normals
            gl::VertexAttribPointer(
                1,
                3,
                gl::FLOAT,
                gl::FALSE,
                6 * mem::size_of::<GLfloat>() as GLint,
                (3 * mem::size_of::<GLfloat>()) as *const GLvoid,
            );

            gl::DrawElements(
                gl::TRIANGLES,
                self.indices.len() as i32,
                gl::UNSIGNED_INT,
                std::ptr::null(),
            );

            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }
    }
}

impl Drop for Solid {
    fn drop(&mut self) {
        let buffers = vec![self.vao, self.vbo, self.vio];

        unsafe {
            gl::DeleteBuffers(buffers.len() as i32, buffers.as_ptr());
        }
    }
}
