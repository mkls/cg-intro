use crate::draw::Draw;
use crate::program::ShaderProgram;
use crate::shader::Shader;
use cgmath::{Angle, Deg, Matrix4, Rad};
use flo_curves::Coord2;
use gl::types::{GLfloat, GLint, GLsizeiptr, GLuint};
use gl::{FRAGMENT_SHADER, TRIANGLE_FAN, VERTEX_SHADER};
use std::env::current_exe;
use std::mem;

pub struct Point {
    pub(super) x: f32,
    pub(super) y: f32,

    vao: GLuint,
    vbo: GLuint,

    vertices: Vec<f32>,

    program: ShaderProgram,
}

impl Point {
    pub fn new(x: f32, y: f32, radius: f32) -> Result<Self, Box<dyn std::error::Error>> {
        let exe_path = current_exe().unwrap().parent().unwrap().join("shaders");

        let vs_path = exe_path.join("point.vert");
        let fs_path = exe_path.join("point.frag");

        let vs = Shader::new(vs_path.to_str().unwrap(), VERTEX_SHADER)?;
        let fs = Shader::new(fs_path.to_str().unwrap(), FRAGMENT_SHADER)?;

        let program = ShaderProgram::new(vec![vs, fs]);

        let vao = 0;
        let vbo = 0;
        let vertices = Self::make_point_vertices(x, y, radius);

        let mut point = Self {
            x,
            y,
            vao,
            vbo,
            vertices,
            program,
        };

        point.make_buffers();

        Ok(point)
    }

    pub fn to_coord(&self) -> Coord2 {
        Coord2(self.x as f64, self.y as f64)
    }

    fn make_point_vertices(x: f32, y: f32, radius: f32) -> Vec<f32> {
        const SPLITS: f32 = 20.0;
        let angle_step = 360.0 / SPLITS;
        let mut vertices = Vec::with_capacity(2 * (SPLITS as usize + 2));

        vertices.push(x);
        vertices.push(y);

        let mut angle = 0.0;
        for _ in 0..=(SPLITS as i32) {
            angle += angle_step;
            let rad_angle = Rad::from(Deg(angle));

            let t_x = x + Angle::cos(rad_angle) / SPLITS / radius / 2.0;
            let t_y = y + Angle::sin(rad_angle) / SPLITS / radius / 2.0;

            vertices.push(t_x);
            vertices.push(t_y);
        }

        // vertices.push(x + radius);
        // vertices.push(y);

        vertices
    }

    fn make_buffers(&mut self) {
        unsafe {
            // Vertices
            gl::GenVertexArrays(1, &mut self.vao);
            gl::BindVertexArray(self.vao);

            gl::GenBuffers(1, &mut self.vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (self.vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                self.vertices.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );

            // Attributes
            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0,
                2,
                gl::FLOAT,
                gl::FALSE,
                2 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null(),
            );
        }
    }
}

impl Draw for Point {
    fn draw(&self, _: Option<&Matrix4<f32>>, _: Option<&Matrix4<f32>>, _: Option<&Matrix4<f32>>) {
        unsafe {
            gl::UseProgram(self.program.handle);

            gl::BindVertexArray(self.vao);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);

            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0,
                2,
                gl::FLOAT,
                gl::FALSE,
                2 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null(),
            );

            gl::DrawArrays(TRIANGLE_FAN, 0, (self.vertices.len() / 2) as i32);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }
    }
}

impl Drop for Point {
    fn drop(&mut self) {
        unsafe {
            // Unbind stuff
            gl::BindVertexArray(0);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);

            // And delete
            gl::DeleteVertexArrays(1, self.vao as *const GLuint);
            gl::DeleteBuffers(1, self.vbo as *const GLuint);
        }
    }
}
