use crate::draw::Draw;
use crate::point::Point;
use crate::program::ShaderProgram;
use crate::shader::Shader;
use cgmath::Matrix4;
use flo_curves::bezier::Curve as Bezier;
use flo_curves::bezier::*;
use gl::types::{GLfloat, GLint, GLsizeiptr, GLuint};
use gl::{FRAGMENT_SHADER, LINE_STRIP, VERTEX_SHADER};
use std::env::current_exe;
use std::io::Error;
use std::mem;

pub struct Curve {
    pub(super) base_points: Vec<Point>,
    pub(super) curve_points: Vec<f32>,
    pub(super) curve_normals: Vec<f32>,
    curve_points_count: u32,
    vao: GLuint,
    vbo: GLuint,
    program: ShaderProgram,
}

impl Curve {
    pub fn new() -> Result<Self, Error> {
        let exe_path = current_exe().unwrap().parent().unwrap().join("shaders");

        let vs_path = exe_path.join("curve.vert");
        let fs_path = exe_path.join("curve.frag");

        let vs = Shader::new(vs_path.to_str().unwrap(), VERTEX_SHADER)?;
        let fs = Shader::new(fs_path.to_str().unwrap(), FRAGMENT_SHADER)?;

        let program = ShaderProgram::new(vec![vs, fs]);

        let base_points = Vec::new();
        let curve_points = Vec::new();
        let curve_normals = Vec::new();

        let vao = 0 as GLuint;
        let vbo = 0 as GLuint;
        let curve_points_count = 20;

        let mut curve = Self {
            base_points,
            curve_points,
            curve_normals,
            curve_points_count,
            vao,
            vbo,
            program,
        };

        curve.make_buffers();

        Ok(curve)
    }

    pub fn add_point(&mut self, point: Point) {
        self.base_points.push(point);
        if self.base_points.len() % 4 == 0 {
            self.update_curve_points();
            self.update_buffers();
        }
    }

    pub fn reset(&mut self) {
        self.base_points.clear();
        self.curve_normals.clear();
        self.curve_normals.clear();

        self.update_buffers();
    }

    fn update_curve_points(&mut self) {
        let mut curve_points: Vec<f32> = Vec::new();
        let mut curve_normals: Vec<f32> = Vec::new();

        for i in (0..self.base_points.len()).step_by(4) {
            let start = self.base_points[i].to_coord();
            let end = self.base_points[i + 3].to_coord();
            let control_1 = self.base_points[i + 1].to_coord();
            let control_2 = self.base_points[i + 2].to_coord();

            let bezier = Bezier::from_points(start, (control_1, control_2), end);

            for j in 0..self.curve_points_count {
                let pos = j as f64 * 1.0 / self.curve_points_count as f64;

                let p = bezier.point_at_pos(pos);
                let n = bezier.normal_at_pos(pos);

                curve_points.push(p.x() as f32);
                curve_points.push(p.y() as f32);

                curve_normals.push(n.x() as f32);
                curve_normals.push(n.y() as f32);
            }
        }

        self.curve_points = curve_points;
        self.curve_normals = curve_normals;
    }

    fn update_buffers(&mut self) {
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (self.curve_points.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                self.curve_points.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );
        }
    }

    fn make_buffers(&mut self) {
        unsafe {
            // Vertices
            gl::GenVertexArrays(1, &mut self.vao);
            gl::BindVertexArray(self.vao);

            gl::GenBuffers(1, &mut self.vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (self.curve_points.len() % 4 * mem::size_of::<GLfloat>()) as GLsizeiptr,
                self.curve_points.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );

            // Attributes
            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0,
                2,
                gl::FLOAT,
                gl::FALSE,
                2 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null(),
            );
        }
    }
}

impl Draw for Curve {
    fn draw(&self, _: Option<&Matrix4<f32>>, _: Option<&Matrix4<f32>>, _: Option<&Matrix4<f32>>) {
        for point in &self.base_points {
            point.draw(None, None, None);
        }

        unsafe {
            gl::UseProgram(self.program.handle);

            gl::BindVertexArray(self.vao);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);

            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0,
                2,
                gl::FLOAT,
                gl::FALSE,
                2 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null(),
            );

            gl::DrawArrays(LINE_STRIP, 0, (self.curve_points.len() / 2) as i32);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }
    }
}

impl Drop for Curve {
    fn drop(&mut self) {
        unsafe {
            // Unbind stuff
            gl::BindVertexArray(0);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);

            // And delete
            gl::DeleteVertexArrays(1, self.vao as *const GLuint);
            gl::DeleteBuffers(1, self.vbo as *const GLuint);
        }
    }
}
