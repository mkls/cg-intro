use crate::shader::Shader;
use gl;
use gl::types::*;
use std::{ptr, str};

/// Shader program handler struct
pub struct ShaderProgram {
    pub(super) handle: GLuint,
}

impl ShaderProgram {
    /// Create new shader program object
    pub fn new(shaders: Vec<Shader>) -> Self {
        let handle = Self::link_shader_program(shaders);

        Self { handle }
    }

    fn link_shader_program(shaders: Vec<Shader>) -> GLuint {
        let handle;

        unsafe {
            handle = gl::CreateProgram();

            for shader in &shaders {
                gl::AttachShader(handle, shader.handle);
            }

            gl::LinkProgram(handle);

            for shader in &shaders {
                gl::DetachShader(handle, shader.handle);
            }

            // Check if shader program linkage failed
            let mut status = gl::FALSE as GLint;
            gl::GetProgramiv(handle, gl::LINK_STATUS, &mut status);

            if status != (gl::TRUE as GLint) {
                let mut len: GLint = 0;
                gl::GetProgramiv(handle, gl::INFO_LOG_LENGTH, &mut len);

                let mut buf = Vec::with_capacity(len as usize);
                buf.set_len((len as usize) - 1);

                gl::GetProgramInfoLog(
                    handle,
                    len,
                    ptr::null_mut(),
                    buf.as_mut_ptr() as *mut GLchar,
                );
                panic!(
                    "{}",
                    str::from_utf8(&buf)
                        .ok()
                        .expect("ShaderInfoLog is not valid utf8")
                );
            }
        }

        handle
    }
}

impl Drop for ShaderProgram {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.handle);
        }
    }
}
