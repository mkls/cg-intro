use crate::gl_helper::{compile_shader, link_shader_program};
use crate::util::*;
use core::ffi::c_void;
use gl::types::*;
use image::io::Reader as ImageReader;
use std::env::current_exe;
use std::ffi::CString;
use std::fs::read_to_string;
use std::mem;

pub struct Drawable {
    vao: GLuint,
    vbo: GLuint,
    vio: GLuint,

    texture1: GLuint,
    texture2: GLuint,

    element_count: u32,

    pub g_mvp: GLint,
    pub g_mv: GLint,

    program: GLuint,
}

impl Drawable {
    pub fn new() -> Self {
        let exe_path = current_exe().unwrap().parent().unwrap().join("shaders");

        // Read shader sources
        let vs_str = read_to_string(exe_path.join("surface.vert")).unwrap();
        let fs_str = read_to_string(exe_path.join("surface.frag")).unwrap();

        let n = 500;

        // Compile shader program for the new drawable
        let vs = compile_shader(vs_str.as_str(), gl::VERTEX_SHADER);
        let fs = compile_shader(fs_str.as_str(), gl::FRAGMENT_SHADER);

        let program = link_shader_program(vs, fs);

        // Generate array and buffer objects
        let mut vao = 0;
        let mut vbo = 0;
        let mut vio = 0;
        let mut texture1 = 0;
        let mut texture2 = 0;

        let g_mvp;
        let g_mv;

        let vertices = gen_vertices(n);
        let indices = gen_indices(n);

        unsafe {
            // Vertices
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);

            gl::GenBuffers(1, &mut vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                vertices.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );

            // Indices
            gl::GenBuffers(1, &mut vio);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, vio);
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (indices.len() * mem::size_of::<GLuint>()) as GLsizeiptr,
                indices.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );

            // Attributes
            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0,
                2,
                gl::FLOAT,
                gl::FALSE,
                2 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null(),
            );

            g_mvp = gl::GetUniformLocation(
                program,
                CString::new("mvp").unwrap().as_bytes_with_nul().as_ptr() as *const i8,
            );

            g_mv = gl::GetUniformLocation(
                program,
                CString::new("mv").unwrap().as_bytes_with_nul().as_ptr() as *const i8,
            );

            // Textures
            // gl::GenTextures(2, vec![&mut texture1, &mut texture2].as_ptr() as *mut GLuint);

            gl::GenTextures(1, &mut texture1);
            gl::GenTextures(1, &mut texture2);

            // Read images
            let img1 = ImageReader::open("./textures/rock.jpg")
                .unwrap()
                .decode()
                .unwrap()
                .to_rgb8();
            let img2 = ImageReader::open("./textures/sand_dunes.jpg")
                .unwrap()
                .decode()
                .unwrap()
                .to_rgb8();

            for (t_unit, texture, img) in vec![
                (gl::TEXTURE0, texture1, img1),
                (gl::TEXTURE1, texture2, img2),
            ] {
                gl::ActiveTexture(t_unit);
                gl::BindTexture(gl::TEXTURE_2D, texture);
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as i32);
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as i32);

                let (width, height) = img.dimensions();
                gl::TexImage2D(
                    gl::TEXTURE_2D,
                    0,
                    gl::RGB8 as i32,
                    width as i32,
                    height as i32,
                    0,
                    gl::RGB,
                    gl::UNSIGNED_BYTE,
                    img.into_raw().as_ptr() as *const c_void,
                );
            }
        }

        Self {
            vao,
            vbo,
            vio,
            texture1,
            texture2,
            g_mvp,
            g_mv,
            element_count: indices.len() as u32,
            program,
        }
    }

    pub fn draw(&self) {
        unsafe {
            gl::UseProgram(self.program);

            gl::BindVertexArray(self.vao);

            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.vio);

            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0,
                2,
                gl::FLOAT,
                gl::FALSE,
                2 * mem::size_of::<GLfloat>() as GLint,
                std::ptr::null(),
            );

            gl::ActiveTexture(gl::TEXTURE0);
            let t1 = gl::GetUniformLocation(
                self.program,
                CString::new("texture1")
                    .unwrap()
                    .as_bytes_with_nul()
                    .as_ptr() as *const i8,
            );
            gl::Uniform1i(t1, 0);

            gl::ActiveTexture(gl::TEXTURE1);
            let t2 = gl::GetUniformLocation(
                self.program,
                CString::new("texture2")
                    .unwrap()
                    .as_bytes_with_nul()
                    .as_ptr() as *const i8,
            );
            gl::Uniform1i(t2, 1);

            gl::DrawElements(
                gl::TRIANGLES,
                self.element_count as i32,
                gl::UNSIGNED_INT,
                std::ptr::null(),
            );

            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }
    }
}

impl Drop for Drawable {
    fn drop(&mut self) {
        let buffers = vec![self.vbo, self.vio];

        unsafe {
            gl::DeleteBuffers(buffers.len() as i32, buffers.as_ptr() as *const GLuint);
            gl::DeleteVertexArrays(1, vec![self.vao].as_ptr() as *const GLuint);
            gl::DeleteProgram(self.program);
            gl::DeleteTextures(
                2,
                vec![self.texture1, self.texture2].as_ptr() as *const GLuint,
            );
        }
    }
}
