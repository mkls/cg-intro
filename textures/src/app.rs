use gl;
use glutin;
use glutin::{
    window::WindowBuilder,
    event::{Event, WindowEvent},
    event_loop::{EventLoop, ControlFlow},
    dpi::LogicalSize,
    ContextBuilder,
    WindowedContext
};
use cgmath::{Matrix4, Point3, Rad, Deg, Vector3, prelude::Matrix};
use std::option::Option;
use std::time::Instant;
use crate::draw::Drawable;

pub struct Application {
    context: Option<WindowedContext<glutin::NotCurrent>>,
    event_loop: Option<EventLoop<()>>
}

pub struct ApplicationBuilder {
    size: LogicalSize<u16>,
    title: String,
    resizable: bool
}

impl ApplicationBuilder {
    pub fn new() -> ApplicationBuilder {
        Self {
            size: LogicalSize::new(800, 600),
            title: String::from("CG task"),
            resizable: false
        }
    }

    pub fn with_size(&mut self, size: LogicalSize<u16>) -> &mut Self {
        self.size = size;
        self
    }

    pub fn with_title(&mut self, title: String) -> &mut Self {
        self.title = title;
        self
    }

    pub fn with_resizable(&mut self, resizable: bool) -> &mut Self {
        self.resizable = resizable;
        self
    }

    pub fn build(&self) -> Application {
        let event_loop = EventLoop::new();

        let window = WindowBuilder::new()
            .with_inner_size(self.size)
            .with_title(&self.title)
            .with_resizable(self.resizable);

        let context = ContextBuilder::new()
            .with_gl(glutin::GlRequest::Specific(glutin::Api::OpenGl, (3, 3)))
            .with_gl_profile(glutin::GlProfile::Core)
            .with_pixel_format(24, 8)
            .with_srgb(false)
            .with_vsync(true)
            .build_windowed(window, &event_loop);

        let actual_window = match context {
            Ok(win) => win,
            Err(err) => panic!("Error occured while creating window.\nReason: {}", err)
        };

        Application {
            context: Some(actual_window),
            event_loop: Some(event_loop)
        }
    }
}

impl Application {
    pub fn run(&mut self) {
        let gl_window = unsafe {
            self.context
                .take()
                .unwrap()
                .make_current()
                .unwrap()
        };

        gl::load_with(|symbol| gl_window.get_proc_address(symbol));

        unsafe {
            gl::Enable(gl::DEPTH_TEST);
        }

        let handler = self.event_loop.take().unwrap();

        let drawable = Drawable::new();

        let model = Matrix4::from_scale(0.45);

        let mut view = Matrix4::look_at_rh(
            Point3::new(0.0, 7.0, -12.0),
            Point3::new(0.0, 0.0, 0.0),
            Vector3::new(0.0, 1.0, 0.0),
        );

        let size = gl_window.window()
                            .inner_size();

        let mut proj = cgmath::perspective(
            Rad::from(Deg(45.0)),
            size.width as f32 / size.height as f32,
            0.1,
            100.0,
        );

        let mut time = Instant::now();

        handler.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Wait;

            match event {
                Event::WindowEvent {
                    event,
                    ..
                } => {
                    match event {
                        WindowEvent::CloseRequested => {
                            *control_flow = ControlFlow::Exit
                        },
                        WindowEvent::Resized(size) => {
                            proj = cgmath::perspective(
                                Rad::from(Deg(45.0)),
                                size.width as f32 / size.height as f32,
                                0.01,
                                100.0,
                            );
                            gl_window.resize(size);
                            unsafe {
                                gl::Viewport(0, 0, size.width as i32, size.height as i32)
                            }
                        }
                        _ => ()
                    }
                },
                Event::RedrawRequested(_) => {

                    view = view * Matrix4::from_angle_y(
                        Rad::from(Deg(time.elapsed().as_millis() as f32 / 50.))
                    );
                    time = Instant::now();

                    let mv = view * model;
                    let mvp = proj * view * model;

                    unsafe {
                        gl::ClearColor(0.7, 0.7, 0.7, 1.0);
                        gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
                        
                        gl::UniformMatrix4fv(drawable.g_mvp, 1, gl::FALSE, mvp.as_ptr());
	                gl::UniformMatrix4fv(drawable.g_mv, 1, gl::FALSE, mv.as_ptr());
                    }

                    drawable.draw();
                    gl_window.swap_buffers().unwrap();
                },
                _ => (),
            }
            gl_window.window().request_redraw();
        });
    }
}
