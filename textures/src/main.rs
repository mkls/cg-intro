mod app;
mod util;
mod gl_helper;
mod draw;

use app::ApplicationBuilder;
use glutin::{
    dpi::LogicalSize
};

fn main() {
    let mut application = ApplicationBuilder::new()
        .with_title(String::from("My app"))
        .with_size(LogicalSize::new(800, 600))
        .with_resizable(true)
        .build();

    application.run();
}
