pub fn gen_vertices(n: usize) -> Vec<f32> {
    let mut vertices = Vec::with_capacity(2 * n * n);
    let mut x: f32 = -10.;
    let mut y: f32 = 10.;
    let step: f32 = 2. * y / ((n - 1) as f32);

    for _ in 0..n {
        for _ in 0..n {
            // print!("{:.3}.{:.3}\t", x, y);
            vertices.push(x);
            vertices.push(y);
            x += step;
        }
        // print!("\n");
        x = -10.;
        y -= step;
    }

    vertices
}

pub fn gen_indices(n: usize) -> Vec<u32> {
    let mut indices = Vec::with_capacity((n - 1) * (n - 1) * 6);

    for i in 0..n-1 {
        for j in 0..n-1 {
            // Left triangle
            indices.push((j + i * n) as u32);
            indices.push((j + 1 + i * n) as u32);
            indices.push((j + (i + 1) * n) as u32);

            // println!("{}, {}, {}", (j + i * n), (j + 1 + i * n), (j + (i + 1) * n));

            // Right triangle
            indices.push((j + (1 + i) * n) as u32);
            indices.push((j + 1 + (1 + i) * n) as u32);
            indices.push((j + 1 + i * n) as u32);
        }
    }

    indices
}
