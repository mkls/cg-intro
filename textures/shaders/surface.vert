#version 330 core

layout(location = 0) in vec2 position;
out vec3 normal;
out vec3 pos;
out vec2 t_coord;

uniform mat4 mvp;
uniform mat4 mv;

// layout(set = 0, binding = 0) uniform UBO {
//     mat4 model;
//     mat4 view;
//     mat4 proj;
// } ubo;

const float alpha = 0.6;
const float beta = 1.1;

float function(vec2 pos) {
    return sin(alpha * pos.x) * cos(beta * pos.y);
}

vec2 grad(vec2 pos) {
    return vec2(alpha * cos(alpha * pos.x) * cos(beta * pos.y),
                -beta * sin(alpha * pos.x) * sin(beta * pos.y));
}

void main() {
    vec4 v_pos = vec4(position.x, function(position), position.y, 1.0);
    vec2 gradient = grad(position);

    pos = vec3(mv * v_pos);
    normal = normalize(transpose(inverse(mat3(mv))) * vec3(gradient.x, 1.0, gradient.y));

    gl_Position = mvp * v_pos;
    t_coord = vec2(position.x, position.y) / 4 + 0.5;
}
