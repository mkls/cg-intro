use std::fs::{create_dir_all, copy};
use std::path::Path;
use std::env::{var, current_dir};

fn main() {
    println!("cargo:rerun-if-changed=shaders");
    // Check profile
    let build_profile = var("PROFILE").unwrap();

    // Check if target directory env is set
    let target_dir = match var("CARGO_TARGET_DIR") {
        Ok(str) => Path::new(&str).join(build_profile.as_str()),
        Err(_) =>
            current_dir()
                .unwrap()
                .join("target")
                .join(build_profile)
    };

    let in_shaders_dir = Path::new(&current_dir().unwrap())
        .join("shaders");
    let out_shaders_dir = Path::new(&target_dir).join("shaders");

    create_dir_all(&out_shaders_dir).unwrap();

    let mut res = copy(in_shaders_dir.join("surface.vert"), &out_shaders_dir.join("surface.vert"));
    match res {
        Ok(_) => (),
        Err(e) => println!("Error! {}", e)
    }

    res = copy(in_shaders_dir.join("surface.frag"), &out_shaders_dir.join("surface.frag"));
    match res {
        Ok(_) => (),
        Err(e) => println!("Error! {}", e)
    }

    // Rebuild only when .rs sources have been changed
    println!("cargo:rerun-if-changed=src");
}
